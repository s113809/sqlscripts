INSERT INTO Liedjes(Titel, Artiest, Album, Jaar)
VALUES
('Stairway to Heaven','Led Zeppelin','Led Zeppelin IV','1971'),
('Rock and Roll','Led Zeppelin','Led Zeppelin IV','1971'),
('Riders on the Storm','The Doors','L.A. Woman','1971'),
('Good Enough','Molly Tuttle','Rise','2017'),
('Outrage for the Execution of Willie McGee','Goodnight, Texas','Conductor','2018'),
('They Lie','Layla Zoe','The Lily','2013'),
('Green Eyed Lover','Layla Zoe','The Lily','2013'),
('Why You So Afraid','Layla Zoe','The Lily','2013'),
('It Ain''t You','Danielle Nicole','Wolf Den','2015'),
('Unchained','Van Halen','Fair Warning','1981');